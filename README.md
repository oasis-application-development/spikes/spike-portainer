Portainer Spike
---


# Background

Docker Desktop has been used by Oasis Research Business Solutions for the past 5 years for local container application development.
It has recently become a subscription only service. We have switched to using the FOSS Rancher Desktop system, see this [spike](https://gitlab.oit.duke.edu/londo003/spike-rancher-desktop).
It does not have a graphical user interface to manage images and containers. [Portainer](https://www.portainer.io/) offers a [FOSS
community edition](https://docs.portainer.io/v/ce-2.11/start/install) system to manage Docker Images.

This spike attempts to install Portainer on a Mac OSX with Apple Silicon.
I used the instructions to `Install Docker on WSL / Docker Desktop`, which
works on Racher Desktop too, since it uses the local docker socket.

# Steps

These will illustrate steps that are not part of the official install linked above.

1. Documented portainer server is an older version. Check the [Portainer/portainer-ce docker hub page for latest tag](https://hub.docker.com/r/portainer/portainer-ce). Do this before you run the first docker command to start the server in the installation
instructions.

2. You must create a new admin account with password on initial login to the portainer server (example: 1LuvR8nch3Rz). This would
need to be stored in a developer's 1password private vault.

3. When I first logged in, I did not see some of the more useful menu items. I clicked on `Home`, then clicked the `local` environment box. That brought up the dashboard, and revealed other menu items such as `Dashboard`, `Containers`, etc.

# Pros

- The UI allows for adding, managing, and removing registries, images, containers, networks, volumes, etc.
- You can even build and import images from the UI
- You can click a container, and stop, start, kill, restart, pause, resume, remove, recreate, and duplicate/edit it.
- You can view logs, inspect, get a shell inside of, or attach to a container
- You can configure docker hub login credentials in the Registries view. It lists the number of pulls performed and the
number remaining before you hit the 6 hour limit

# Cons

- The CE edition is currently [FOSS](https://github.com/portainer/portainer). Their primary business model seems to be to stear
users toward their Subscription Business Edition. This does create a risk that they may pursue the same strategy that Docker Desktop
did if they do not get enough voluntary uptake of BE licensees.

- The main interface is a web UI. This requires a dedicated browser window/tab that can get cluttered in with other windows/tabs

- Must manage the portainer container over time. It may not start automatically
with Rancher Desktop restarts, especially if RD is reset.

# Recommendation

Recommended for use until they decide to change their CE support and business model. This would not be as disruptive to our work
as the Docker Desktop change, as it is still possible to do docker and docker-compose CLI commands. As we move to using Kubernetes
for local development, Lens Desktop, which is managed by Mirantis under an MIT License, will provide an alternative well.


